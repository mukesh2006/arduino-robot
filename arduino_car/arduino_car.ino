/*
 * Description:
 * ------------
 * Function: avoid obstacles
 * 
 * Two Parts: 
 * 1. setup() : Initialize the robort with correct parameters! 
 * 2. loop()  : keep strolling and avoid any obstacle in the path!
 *
 * Other Important functions:
 * scanClear(): scans if the path is clear 
 * lookForward(): look forward and senses the average distance of obstacle ahead. 
 *    Case 1: if any distance is below the COLLISION_DISTANCE, it sets the clear flag to FALSE and continues
 *    Case 2: if a distance is below the COLLISION_DISTANCE and below the EMERGENCY_DISTANCE then it directly returns FALSE and exits the function so that the correct reaction takes place.
 *    Case 3: it finishes the analysis and sets the clear flag to TRUE. Then, it checks the average distance on the left and right of the data gathered by looking forward.
 * If the leftDistance is less than the AVOIDANCE_DISTANCE and the rightDistance is greater than the AVOIDANCE_DISTANCE, then the bot veers right.
 * Else if the rightDistance is less than the AVOIDANCE_DISTANCE and the leftDistance is greater than the AVOIDANCE_DISTANCE, then the bot veers left.
 *
 * Acknowledgement: this algorithm is based on the functions and sample sketch provided by oddWires with the purchase of the oddWires Arduino Kit.
 *
 */

//*********************************************************
//* Serial
//*
//* Define the baudrate for the serial output if connected
//*********************************************************
#define BAUDRATE  9600

//*********************************************************
//* Ultrasonic sensor
//* 
//* HC-SR04 Ultrasonic Module         
//*         
//* Vcc +5V     
//* Trig  A4  on the motor shield   
//* Echo  A5  on the motor shield   
//* GND
//**********************************************************

#include <NewPing.h>

#define TRIG_PIN  A4
#define ECHO_PIN  A5

#define MAX_DISTANCE 300                         // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

NewPing sonar(TRIG_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pin and maximum distance.

// The dangerThreshold is used when the bot is trying to decide whether to go left or right
float dangerThreshold = 20; //25.0;                      // 25 cm - depends on the floor surface and speed setting
#define COLLISION_DISTANCE  70 // 60                    // in cm - depends on what the user wants and the floor surface and speed setting (keep in mind that the bot does not stop immidiately)
#define AVOIDANCE_DISTANCE  110 // 100                   // in cm - depends on what the user wants and the floor surface and speed setting (keep in mind that the bot does not stop immidiately)
#define EMERGENCY_DISTANCE  20 // 15                    // in cm - used when the bot is very close to the obstacle and needs to react as soon as it sees it.

//************************************************************ 
//* Servo
//* connect servo to pin 2
//************************************************************
#include <Servo.h> 

#define LEFT         180
#define CENTER       100
#define RIGHT        20

#define SERVO_PIN    2

Servo ultrasonicServo;  // create servo object to control the servo 

int currentPos = 0;    // variable to store the servo position

//**************************************************************
//* Motors
//*
//* Throttle is a number between 128 and 255 -
//* less than 128 any the motors may not have enough torque to move
//* motor1 (left) is connected to motor 1 pins M1
//* motor2 (right)is connected to motor 2 pins M2
//**************************************************************

const int ENA = 10; //6;
const int IN1 = 9; //8;
const int IN2 = 8; //7;
const int IN3 = 6; //2;
const int IN4 = 7; //4;
const int ENB = 5; //3;

int speed_left;
int speed_right;
int driveTime = 150; // drive time for one unit!

//**************************************************************************************************************
//*
//* structure to hold ultrasonic readings
//*
//**************************************************************************************************************

struct angleVector{
  int angle;
  int distance;
};

#define READINGS 5
struct angleVector angleVectors[READINGS];

//*************************************************************
//* Setup
//*************************************************************
void setup() {

  // setup serial communication  
  Serial.begin(BAUDRATE);

  // setup servo
  ultrasonicServo.attach(SERVO_PIN);  // attaches the servo on pin 2

  // move servo to center 
  servo_position(CENTER);  
  delay(3000);
  
  // set motor speed
  speed_left = 255;
  speed_right = 255;
  setSpeed(speed_left, speed_right);
  
  // set all the motor control pins to outputs
  pinMode(ENA, OUTPUT);
  pinMode(ENB, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  
  Serial.println("Setup Done");
}

//*************************************************************
//* Main
//*************************************************************

void loop() {
    if (scanClear())  // If clear to go forward
    { 
       Serial.println("Loop: ScanClear: drive_forward\n ");
       drive_forward();
    }
    else              // if path is blocked
    {
       Serial.println("Loop: ScanClear: freewheel\n ");
      freewheel();    // stop
      
      // look left
      Serial.println("Loop: look left \n ");
      lookLeft();
  
      int distanceLeft = getAverageDistance();
      Serial.println("distanceLeft");
      Serial.println(distanceLeft);
     
      Serial.println("Loop: look right \n ");
      // look right
      lookRight();
      int distanceRight = getAverageDistance();
      Serial.println("distanceRight");
      Serial.println(distanceRight);
      
      // re-center the ultrasonic
      Serial.println("Loop: servo center \n ");
      servo_position(CENTER);

      // go the least obstructed way
      if (distanceLeft > distanceRight && distanceLeft > dangerThreshold)       //if left is less obstructed 
      {
        // back up
          Serial.println("Loop:  Drive Backward 1 : drive_backward () ");
         drive_backward();
         delay(400);

        // rotate left        
        Serial.println("Loop: Rotate Left: rotate_left()");
        rotate_left();
      }
      else if (distanceRight > distanceLeft && distanceRight > dangerThreshold) //if right is less obstructed 
      {
        // back up
        Serial.println("Loop:  Drive Backward 2: drive_backward () ");
        drive_backward();
        delay(400);

        // rotate right
        Serial.println("Loop: Rotate Right: rotate_right()");
        rotate_right();
      }
      else // equally blocked or less than danger threshold left or right
      {
        Serial.println("Equally blocked or less than danger threshold left or right");
        // stop
        freewheel();
        delay(20);
        
        // back up
        Serial.println("Loop: Drive Backward: drive_backward()");    
        drive_backward();
        delay(500);
        
        // make a u-turn
        Serial.println("Loop: Make U-turn: u_turn()");    
        u_turn();
      }   
    } 
  }



//*************************************************************
//* Servo Control
//*************************************************************

void servo_position(int newPos){
  if (newPos > currentPos){
    for(int pos=currentPos; pos < newPos; pos += 1)  // goes from 0 degrees to 180 degrees 
    {                                                // in steps of 1 degree 
      ultrasonicServo.write(pos);                    // tell servo to go to position in variable 'pos' 
      delay(15);                                     // waits 15ms for the servo to reach the position 
    }
    currentPos = newPos;  
  }
  else if (newPos < currentPos){
    for(int pos=currentPos; pos > newPos; pos -= 1)  // goes from 0 degrees to 180 degrees 
    {                                                // in steps of 1 degree 
      ultrasonicServo.write(pos);                    // tell servo to go to position in variable 'pos' 
      delay(15);                                     // waits 15ms for the servo to reach the position 
    }
    currentPos = newPos;  
  }
}

//*************************************************************
//* Ultrasonic Ping
//*************************************************************

float ping(){
  delay(50);                        // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
  unsigned int uS = sonar.ping();   // Send ping, get ping time in microseconds (uS).
  if (uS == 0)  {                    // out of range (0 = outside set distance range, no ping echo)
    //Serial.println("Ping Sonar 1 : ");
    // Serial.println(uS);
    return MAX_DISTANCE;
  }
  else{                              // in range
     // Serial.println("Ping Sonar 1 : ");
     // Serial.println(uS);  
  return uS / US_ROUNDTRIP_CM;      // Convert ping time to distance 
  }
}

//**************************************************************
//* Setup paramteres. For short pile carpet - low battery
//*
//* Vary these params depending on the surface and speed setting
//**************************************************************
#define BRAKE_ACT_TIME   200
#define TURN_ACT_TIME    750
#define ROTATE_ACT_TIME  400 // 820
#define ROTATE_ACT_TIME_RIGHT  400 //900
#define UTURN_ACT_TIME   780

// To stop
void freewheel(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);  
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);
  delay(BRAKE_ACT_TIME);
}

// To stop left wheel
void freewheel_left(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);  
  delay(BRAKE_ACT_TIME);
}

// To stop right wheel
void freewheel_right(){
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);  
  delay(BRAKE_ACT_TIME);
}

//To rotate
void rotate(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);  
  analogWrite(ENA, speed_left);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  analogWrite(ENB, speed_right);
  delay(BRAKE_ACT_TIME);
}

//To drive backward
void drive_backward(){
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENA, speed_left);
  // turn on motor B
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENB, speed_right);
  delay(driveTime);
}

//To drive forward
void drive_forward(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENA, speed_left);
  // turn on motor B
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENB, speed_right);
  delay(driveTime);
}

//To drive forward left
void drive_forward_left(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENA, speed_left);
  delay(driveTime);
}

//To drive forward left
void drive_forward_right(){
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENB, speed_right);
  delay(driveTime);
}

//To rotate from left
void rotate_left(){
  // Run motor 1 backward
   digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENA, speed_left);

 // Run motor 2 forward
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENB, speed_right);
  delay(ROTATE_ACT_TIME);
  freewheel();
  delay(100);
}

void rotate_right(){
 // Run motor 1 forward
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENA, speed_left);

 // Run motor 2 backward
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENB, speed_right);
  delay(ROTATE_ACT_TIME_RIGHT);
  freewheel();
  delay(100);
}

void u_turn(){
   // Run motor 1 backward
   digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENA, speed_left);

 // Run motor 2 forward
    digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  // set speed to 200 out of possible range 0~255
  analogWrite(ENB, speed_right);
  delay(UTURN_ACT_TIME);
  freewheel();
  delay(100);
  }

 // To veer left
void veer_left(){
  //Stop left wheel (motor 1);
  freewheel_left();
  //drive right wheel forward (motor 2);
  drive_forward_right();
  delay(TURN_ACT_TIME/2);
}

// To veer right
void veer_right(){
// stop right wheel
  freewheel_right();
// Run motor 2 forward
  drive_forward_left();
  delay(TURN_ACT_TIME/2);
}
//**************************************************************************************************************
//*
//* Set Speed function
//*
//* You can adjust SPEED_OFFSET to allow for differences between the motor speeds
//* Make it negative if the robot veers to the right and positive if it veers to the left
//*
//**************************************************************************************************************
#define SPEED_OFFSET - 10      // this offset is specific to your motor set - adjust till you get a straight path
#define SPEED_CHANGE_TIME 10  // time in milliseconds to react

void setSpeed(int speed_left_temp, int speed_right_temp){
  analogWrite(ENA, speed_left_temp);
  analogWrite(ENB, speed_right_temp);
  delay(SPEED_CHANGE_TIME);
}


//*************************************************************************************************************
//*
//* Scanning Functions
//*
//**************************************************************************************************************

// scanClear()
bool scanClear(){

  bool clear = true;
  bool lowValue = false;

  lookForward();
  for (int i = 0; i < READINGS; i++){
    if (angleVectors[i].distance <= COLLISION_DISTANCE)     //if path is not clear
    {
      lowValue = true;

      if (lowValue)
      {
        clear = false;
        if (angleVectors[i].distance <= EMERGENCY_DISTANCE) // if too close abort
        {
          return clear;
        }
      }
    }
  }

  // If it is OK to go forward
  if (clear){                     // not going to collide 
    //Serial.println("\n OK TO Go FORWARD \n");
    // see if we need to veer 
    int leftDistance = averageLeftDistance();
    int rightDistance = averageRightDistance();
       
    if (leftDistance < AVOIDANCE_DISTANCE && rightDistance > AVOIDANCE_DISTANCE){
      veer_right(); 
    }
    else if (rightDistance < AVOIDANCE_DISTANCE && leftDistance > AVOIDANCE_DISTANCE){
      veer_left(); 
    }      
  }
  return clear;
}

 // scan()
void scan(){

  for(int i = 0; i < READINGS; i++){     // loop to sweep the servo (& sensor) 

    angleVectors[i].distance = ping();
    
    if (angleVectors[i].distance <= 15) // if too close abort
    {
      angleVectors[i].distance = ping();                  // ping again to make sure it is not a noise value
      
      if (angleVectors[i].distance <= EMERGENCY_DISTANCE) // if too close abort
      {
        ultrasonicServo.write(CENTER);                   // set servo to face the starting point
        return;                                          // quit
      }
    }
    
    ultrasonicServo.write(angleVectors[i].angle);        // set servo position
    delay(35);                                           // wait 35 milliseconds for servo to reach position 
  }
  ultrasonicServo.write(CENTER);                         // set servo to face the starting point
  delay(35);                                             // wait 35 milliseconds for servo to reach position
}

//*************************************************************************************************************
//*
//* look Functions
//*
//* Note: The angles are chosen relative to the sonar position on top of the servo.
//*       Can be modified depending on the center position of the servo and the center position of the sonar.
//*       For the lookForward() function, 100-140 scans in front of the bot.
//*       For the lookLeft() function 140-180 scans to the left of the bot.
//*       For the lookRight() function 0-40 scans to the right of the bot.
//**************************************************************************************************************

void lookForward(){
  // set up the reading angles for the servo
  angleVectors[0].angle = 60; //100;
  angleVectors[1].angle = 80; //110;
  angleVectors[2].angle = 100; //120;
  angleVectors[3].angle = 120; //130;
  angleVectors[4].angle = 140; //140;
  ultrasonicServo.write(angleVectors[0].angle); // set servo to face the starting point
  delay(300);                                   // wait 300 milliseconds for servo to reach position
  scan();
} 

void lookLeft(){
  // set up the reading angles for the servo
  angleVectors[0].angle = 140;
  angleVectors[1].angle = 150;
  angleVectors[2].angle = 160;
  angleVectors[3].angle = 170;
  angleVectors[4].angle = 180;
  ultrasonicServo.write(angleVectors[0].angle);           // set servo to face the starting point
  delay(300);                                             // wait 300 milliseconds for servo to reach position
  scan();
}  

void lookRight(){
  // set up the reading angles for the servo
  angleVectors[0].angle = 0;
  angleVectors[1].angle = 10;
  angleVectors[2].angle = 20;
  angleVectors[3].angle = 30;
  angleVectors[4].angle = 40;  
  ultrasonicServo.write(angleVectors[0].angle);           // set servo to face the starting point
  delay(500);                                             // wait 500 milliseconds for servo to reach position
  scan();
}  

//*************************************************************************************************************
//*
//* Averaging Functions
//*
//**************************************************************************************************************

int averageRightDistance(){
  int accumulator = 0;
  for (int i = 0; i < 2; i++){
    accumulator += angleVectors[i].distance;
  }
  return (accumulator / 2);  
}  

int averageLeftDistance(){
  int accumulator = 0;
  for (int i = 3; i < 5; i++){
    accumulator += angleVectors[i].distance;
  }
  return (accumulator / 2);  
}  

int getAverageDistance(){
  int accumulator = 0;
  for (int i = 0; i < READINGS; i++){
    accumulator += angleVectors[i].distance;
  }
  return (accumulator / READINGS);
}
// End of program
